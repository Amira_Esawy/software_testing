import org.junit.Assert;

public class TestMerge {
	@org.junit.Test
	public void constructorTest() {
		int [] Arr1= {4,5,6};
		int [] Arr2= {7,9,10};
		int [] Arr3= {1,2,3};
		int [] Arr4= {4,5,6};
		
		int [] Arr5= {};
		int [] Arr6= {9,8,6};
		int [] Arr7= {1,2,3};
		int [] Arr8= {};
		int [] Arr9= {};
		int [] Arr10= {};
		
		int [] output1= MergeUniqueArrays.mergeArrays(Arr1, Arr2);
		int [] output2= MergeUniqueArrays.mergeArrays(Arr3, Arr4);
		int [] output3= MergeUniqueArrays.mergeArrays(Arr5, Arr6);
		int [] output4= MergeUniqueArrays.mergeArrays(Arr7, Arr8);
		int [] output5= MergeUniqueArrays.mergeArrays(Arr9, Arr10);
		
		
		int [] expected1= {4,5,6,7,9,10};
		int [] expected2= {1,2,3,4,5,6};
		int [] expected3= {9,,8,6};
		int [] expected4= {1,2,3};
		int [] expected5= null;
		
		Assert.assertArrayEquals(expected1, output1);
		Assert.assertArrayEquals(expected2, output2);
		Assert.assertArrayEquals(expected3, output3);
		Assert.assertArrayEquals(expected4, output4);
		Assert.assertArrayEquals(expected5, output5);

}
}
