import org.junit.Assert;

public class Test {
@org.junit.Test 
public void constructorTest() {
	Patient p= new Patient("Mohamed", "Mostafa");
	Assert.assertTrue(p.getfname().equals("Mohamed")); 
	Assert.assertTrue(p.getlname().equals("Mostafa")); 
	
	p.setSSN("ssn569");
	Assert.assertTrue(p.getSSN().equals("ssn569"));
	
	p.setPhNum("0105894637");
	Assert.assertTrue(p.getPhNum().equals("0105894637"));
}
}
